CREATE DATABASE rezodb1 DEFAULT CHARACTER SET utf8;

USE rezodb1;

CREATE TABLE item_category (
  category_id int primary key not null AUTO_INCREMENT ,
  category_name varchar(256) not null
  );
