CREATE DATABASE rezodb2 DEFAULT CHARACTER SET utf8;

USE rezodb2;

CREATE TABLE item(
    item_id int primary key not null AUTO_INCREMENT,
    item_name VARCHAR(256) not null,
    item_price int not null,
    category_id int
    );
