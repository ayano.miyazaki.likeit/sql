SELECT x.category_name,SUM(y.item_price)AS total_price
FROM rezodb1.item_category as x
left join rezodb2.item as y
on y.category_id = x.category_id
GROUP BY y.category_id
order by total_price DESC;
