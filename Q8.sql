SELECT y.item_id, y.item_name, y.item_price, x.category_name
FROM rezodb2.item as y
left join rezodb1.item_category as x
on y.category_id = x.category_id
order by y.item_id;
